# Workshop Slides


This repo contains the slides (in german) of the workshop:

> **Nachhaltigkeit trotz und durch Digitalisierung**
> Hintergründe und Handlungsoptionen auf individueller und struktureller Ebene



Slides can be viewed here:

https://hackmd.okfn.de/p/HJMSms6dn#/0
