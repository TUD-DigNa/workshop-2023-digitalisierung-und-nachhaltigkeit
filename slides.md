---
slideOptions:
  transition: fade
  theme: moon
  center: false
  width: 1800
  height: 1250


---

<style type="text/css">
  .reveal p {
    text-align: left;
  }
  .reveal h1, h2, h3, h4 {
    text-align: left;
  }
  .reveal ul {
    display: block;
  }
  .reveal ol {
    display: block;
  }
</style>




<br><br><br>





# <div style="text-align: center;"> Nachhaltigkeit trotz und durch Digitalisierung </div>

<br>


### <div style="text-align: center;"> Hintergründe und Handlungsoptionen auf individueller und struktureller Ebene </div>


### <div style="text-align: center;"> (Workshop) </div>

<br><br>


#### <div style="text-align: center;"> (Carsten Knoll, AG DigNa (KU, TUD); Bits&Bäume) </div>

#### <div style="text-align: center;"> (Felina Holländer, AG DigNa) </div>



---

## Agenda

<br>

- Vorstellung
- Begriffe
    - Digitalisierung
    - Nachhaltigkeit
- Problemanalyse
- Handlungsoptionen
    - Allgemein
    - Strukturell
    - Individuell

<br><br>

## Vorbemerkungen <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
- <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  Workshop-Charakter: Bitte um Live-Feedback (inkl. Folien-Anpassung)
- <!-- .element: class="fragment fade-up" data-fragment-index="1" --> Triggerwarnung: "Ende der Menschheit"



---

## "Digitalisierung"

- <!-- .element: class="fragment" data-fragment-index="1" --> Eigentlich:
    - ![](https://hedgedoc.c3d2.de/uploads/1183d3a0-31d1-4c29-8786-882b54555fb5.jpeg)
    - Umwandlung von analog verfügbaren Informationen digitale (quantisierte) = Analog-Digital-Wandlung
    - von lateinisch digitus "Finger" und englisch digit "Ziffer"
- <!-- .element: class="fragment" data-fragment-index="2" --> Tatsächlich (hier und meist): im Sinne von "Digitale Transformation"
  > <!-- .element: class="fragment" data-fragment-index="2" -->
  > [beschreibt] einen fortlaufenden, tiefgreifenden Veränderungsprozess in Wirtschaft und Gesellschaft, der durch die Entstehung immer leistungsfähigerer digitaler Techniken und Technologien ausgelöst worden ist [WP1](https://de.wikipedia.org/wiki/Digitale_Transformation)


---

## "Nachhaltigkeit"

- Ursprünglich aus der Forstwirtschaft (Tharandt, 1713) <!-- .element: class="fragment" data-fragment-index="0" -->
    - (nur so viele Bäume fällen, wie im gleichen Zeitraum nachwachsen)
- Handlungsprinzip bei der Nutzung von Ressourcen. <!-- .element: class="fragment" data-fragment-index="2" -->
- <!-- .element: class="fragment" data-fragment-index="2" --> Ziel:
    >"dauerhafte Bedürfnisbefriedigung gewährleisten, durch Bewahrung der natürlichen Regenerationsfähigkeit der beteiligten Systeme" [[WP2](https://de.wikipedia.org/wiki/Nachhaltigkeit)] <!-- .element: class="fragment" data-fragment-index="2" -->
- <!-- .element: class="fragment" data-fragment-index="3" --> Oft: Synonym für "Zukunftsverträglichkeit"
  >  Entwicklung, die die Bedürfnisse der Gegenwart befriedigt, ohne zu riskieren, daß künftige Generationen ihre eigenen Bedürfnisse nicht befriedigen können. [[WP3](https://de.wikipedia.org/wiki/Brundtland-Bericht)] <!-- .element: class="fragment" data-fragment-index="3" -->
- <!-- .element: class="fragment" data-fragment-index="3" -->
  → Sustainable Development Goals (SDGs) der UN: https://sdgs.un.org/goals

---

# Problemanalyse

![](https://dresden.bits-und-baeume.org/assets/slides/ds19bub/p/fff.jpg)

---

### Ökologische und gesellschaftliche Probleme zunehmend unübersehbar

- Destabilisierung des Klimasystems  <!-- .element: class="fragment" data-fragment-index="1" -->
- Artensterben  <!-- .element: class="fragment" data-fragment-index="1" -->
- Verschmutzung<br><br><br>  <!-- .element: class="fragment" data-fragment-index="1" -->
- Ressourcenverknappung  <!-- .element: class="fragment fade-up" data-fragment-index="2" -->
- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> Polarisierung
- Erstarken von Autokratie (Überwachung, ...)  <!-- .element: class="fragment fade-up" data-fragment-index="2" -->
- Menschenrechtsverletzungen  <!-- .element: class="fragment fade-up" data-fragment-index="2" -->
- →Erosion zivilisatorischer Errungenschaften  <!-- .element: class="fragment fade-up" data-fragment-index="2" -->

---

### Digitalisierung: "Brandbeschleuniger"

![](https://dresden.bits-und-baeume.org/assets/slides/ds19bub/p/brandbeschleuniger.jpeg =x1000)


---

### Konkrete Wirkungen:

<br><br><br>

- Stofflich
- Energetisch
- Sozial
- Gesellschaftlich
- Evolutionär


---

### Stoffliche Dimension (1):

<br><br><br>

- Digitalisierung braucht Infrastruktur (Hardware)
- Hardware-Herstellung braucht Ressourcen
    - Bergbau (per Definition nicht nachhaltig)
    - Transport (mehr Infrastruktur)
    - Verarbeitung (Bsp: neue Chip-Fabrik bei Magdeburg)

---

### Stoffliche Dimension (2):

Spezifisches Problem der Digitalisierung: Moorsches Gesetz (exp. Wachstum)


![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Moore%27s_Law_Transistor_Count_1970-2020.png/1024px-Moore%27s_Law_Transistor_Count_1970-2020.png =x1000)


---

### Stoffliche Dimension (3):

<br><br><br>

- → technische Obsoleszenz
- sehr kurze Produktlebenszyklen


---

### Energetische Dimension (1):

<br><br><br>

- elektrische Energie benötigt:
    - Geräte
    - Infrastruktur (Rechenzentren, Netze)

<br><br>
- <!-- .element: class="fragment fade-up" data-fragment-index="2" -->
  Quiz:
      - Wie viel Leistung (Watt) hat ein Notebook?
      - Was bedeutet das im Vergleich zum Mensch (Ergometer)?

<br><br>
- <!-- .element: class="fragment fade-up" data-fragment-index="3" -->
  2018: 50.000 dt. Rechenzentren → 14 TWh verbraucht, Tendenz ↑↑ [[Focus 1](https://www.focus.de/digital/dldaily/dldaily-gastbeitrag-von-eric-waltert-dark-data-vergessene-daten-verschwenden-den-strom-einer-millionenstadt_id_13025634.html?xing_share=news)]
    - (> Berlin)

---

### Energetische Dimension (2):

<br><br><br>

Probleme:

- Hoher CO2-Anteil im Strommix → Klima
    - Ziel: Absolute Emissionen ↓↓
- Eneuerbare Energien sind keine Universallösung
    - Produktion, Flächenkonkurrenz, Transport

---

### Soziale Dimension

<br><br><br>

- Suchtverhalten (Spiele, Streaming)
- Kurzzsichtigkeit (zu häufige Fokussierung auf Nahbereich)
- Konzentrationsstörungen (Aufmerksamkeitsspanne)
- Vereinsamung (weniger "echte" soziale Interaktion)
- Kommunikationsstörungen (weniger "echte" Gespräche)

---

### Gesellschaftliche Dimension


<br>


- <!-- .element: class="fragment fade-up" data-fragment-index="1" --> dysfunktionale Diskurse
    - Filterblasen
    - Echokammern
    - Schreispiralen

<br><br>
- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> Zerstörung demokratischer Prinzipien
    - Massenüberwachung (Datensammeln)
    - individualisierte Massenbeeinflussung
    - → eingeschränkte Selbstbestimmung


<br><br>
- <!-- .element: class="fragment fade-up" data-fragment-index="3" -->
  Lehren aus der Geschichte:
    - Wissen ist Macht
    - Machtkonzentration ist schlecht (also ernsthaft)

---

### Evolutionäre Dimension ("starke KI")

<br>

- Darwin Award
  >  sarkastischer Negativpreis [...]. Er wird seit 1994 dazu verwendet, um über Menschen zu berichten, die sich selbstverschuldet töten, tödlich verunfallen oder selbst unfruchtbar machen und dabei ein besonderes Maß an Dummheit zeigen.

- <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  Menschheit insgesamt arbeitet aktuell an einer umfangs- und aussichtsreichen Bewerbungsmappe

<br>

- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> "Starke KI": künstliches System mit allgemeiner Problemlösekompetenz
- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> Kipppunkt: Wenn sich das System aktiv selbstständig verbessern kann

<br>

- <!-- .element: class="fragment fade-up" data-fragment-index="3" -->
  → Polarisierendes Thema
    - Gefahr 1: Ablenkung von aktuell relevanteren Gefahren
    - Gefahr 2: "Boiling the frog" → Ignorieren der Warnsignale

---

# Chancen-Analyse
## (Digitalisierung → Nachhaltigkeit)

<br><br><br>

- <!-- .element: class="fragment fade-up" data-fragment-index="1" --> Sparen von Resourcen
    - Bildschirm statt Papier
    - Reduktion von Mobilitätsnotwendigkeit (Online-Konferenzen)
    - Wiederverwertung (z.B. Ebay )
    - Effizienzpotentiale (z.B. Gebäudesteuerung, Car-Sharing, ...)

- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> Zugang zu Informationen
    - Wikipedia, Wikibooks, Wikidata, ...
- <!-- .element: class="fragment fade-up" data-fragment-index="3" --> potentiell bessere Demokratiemöglichkeiten
    - Beispiel: toolbasiertes systemisches Konsensieren [[moodpoll](https://moodpoll.uber.space/)]

<br><br>

- <!-- .element: class="fragment fade-up" data-fragment-index="4" -->
  Aktuell: Chancen werden von Risiken dominiert

---

# Handlungsoptionen
## Allgemeines

<br><br>

- <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  Digitalisierung ist kein Selbstzweck!
    - Kritisches Hinterfragen: Warum? und Wie?
- <!-- .element: class="fragment fade-up" data-fragment-index="2" -->
  problematische Aspekte kennen
    - Regelmäßig reflektieren (1. Weiterentwicklungen, 2. Vergessen)
- <!-- .element: class="fragment fade-up" data-fragment-index="3" -->
  problematische Aspekte meiden
    - Digitale Suffizienz
    - \+ Gesunder Menschenverstand
    - \+ digitale Allgemeinbildung

---

## Digitale Suffizienz

<br><br>

- Begriff "Suffizienz":
    - von lat. "sufficere" → ≈"ausreichen"
    - im Zsh. mit Nachhaltigkeit: das richtigen Maß, also "Angemessenheit"
    - absolute Größen relevant (z.B. Gesamtemissionen)
    - Orthogonales Konzept zu Effizienz (relative Größen)

<br>

- <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  digitale Suffizienz [[LSZ 2019](http://www.santarius.de/2048/digitale-suffizienz/)]:
    - "So viel Digitalisierung wie nötig, so wenig wie möglich"
    - Hardwaresuffizienz
    - Softwaresuffizienz
    - Nutzungssuffizienz
    - ...

- <!-- .element: class="fragment fade-up" data-fragment-index="2" -->
  Wichtiger Teilaspekt: **Datensparsamkeit**
    - betrifft Software (Entwicklung, Beschaffung, Nutzung)

---


## Exkurs: (digitale) Werbung

<br><br>

- Ziele von Werbung
    - <!-- .element: class="fragment fade-up" data-fragment-index="1" -->Bedürfnisse spezifizieren:
        - "Ich möchte was trinken." → "Ich möchte eine Coca Cola."
    - <!-- .element: class="fragment fade-up" data-fragment-index="2" -->neue Bedürfnisse erzeugen:
        - "Ich bin zufrieden." → "Ich möchte neue Nike-Schuhe."
        - → nicht nachhaltig!

<br><br>

- <!-- .element: class="fragment fade-up" data-fragment-index="3" -->
  individualisierte Werbung funktionert massiv besser
      - Interessen und Schwächen der Menschen kennen → Angebote die man nicht ablehnen kann
- <!-- .element: class="fragment fade-up" data-fragment-index="3" -->
  → Billionen-Potenzial (siehe Börsenwert von Google, Facebook, Amazon)
- <!-- .element: class="fragment fade-up" data-fragment-index="4" -->
  Erzeugen neuer Bedürfnisse → Gegenteil von Nachhaltigkeit
  <br>
- <!-- .element: class="fragment fade-up" data-fragment-index="5" -->
  Außerdem: Werbung (insb. Videos) verursacht >90% des Traffics (Ladezeit, Akkuverbrauch, ...)

---

![](https://hedgedoc.c3d2.de/uploads/29646e16-d335-4cb2-997c-4add52a7d4a7.jpg)


**→ Der Treiber der existenziellen ökologischen Krise ist <u>legale</u> wirtschaftliche Aktivität!** <!-- .element: class="fragment fade-up" data-fragment-index="1" -->

---

<br><br><br>

Banale aber unbequeme Erkenntnis:

<br>

## *Nachhaltigkeit erfordert die Einstellung aller nicht-nachhaltigen Geschäftsmodelle.*


---

## Digitale Allgemeinbildung (Datensparsamkeit, Grafikformate)

### Motivation

<br><br><br>

- 03.05.2023: Mail an (vermutlich alle ca. 5000) TU-MA:
- Inhalt: Veranstaltungsinfo Tag der Gesundheit der TUD
    - Anhang 1: PDF (90 kB)
    - Anhang 2: PPTX (2,1 MB) mit gleichem Inhalt

<br>

# <!-- .element: class="fragment fade-up" data-fragment-index="1" --> 🤦


<br><br>

Auch sonst: Umgang mit digitalen Bild-Daten weit weg von Optimalität <!-- .element: class="fragment fade-up" data-fragment-index="2" -->


---

## Crash-Kurs: Digitale Bildverarbeitung (1)


![](https://hedgedoc.c3d2.de/uploads/bd6da828-f12b-4f96-bd9f-7b40d8e51e75.png =x450) <!-- .element: class="fragment fade-up" data-fragment-index="1" -->

### Rastergrafiken

- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> bestehen aus Pixeln (z.B. 800x600); für jedes Pixel werden Farbwerte gespeichert
- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> gut für Fotos ...
- <!-- .element: class="fragment fade-up" data-fragment-index="2" --> Formate: PNG, JPG, (BMP), WebP, AviF...

<br>

### Vektorgrafiken

- <!-- .element: class="fragment fade-up" data-fragment-index="3" --> beschreiben Geometrien (z.B. Kreis, Mittelpunkt, Radius)
- <!-- .element: class="fragment fade-up" data-fragment-index="3" --> gut für Text, Logos, ...
- <!-- .element: class="fragment fade-up" data-fragment-index="3" --> Formate: SVG, PDF


---

## Crash-Kurs: Digitale Bildverarbeitung (2)

- Moderne Kameras haben sehr hohe Auflösung
- Bilder sind sehr groß → Quiz: wie groß?
- → Nicht geeignet für direkte digitale Verbreiung (ggf. millioinenfache Übertragung)

<br>

- <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  Möglichkeiten:
    - Auflösung reduzieren (Richtgröße: 1000x750 reicht meist völlig)
    - Kompression (JPG-Qualität → Verlustbehaftet → Richtwert 85%)

![](https://hedgedoc.c3d2.de/uploads/fcb561e3-4f1f-4149-a8bd-4adb6a612443.png)
 <!-- .element: class="fragment fade-up" data-fragment-index="2" -->


---

## Crash-Kurs: Digitale Bildverarbeitung (3)

<br><br><br>

- Wichtig: Andere Prioritäten zwischen Druck und Online-Verbreitung
- Druck: (fast) kompromissloser Fokus auf Qualität
- Online: guter Kompromiss zwischen Qualität und Datenaufwand
    - Energie
    - Ladezeit (→ Nutzungserfahrung)
    - Kosten
<br>
- Richtgröße für "normales" Foto (z.B. Porträt)
    - Quiz:

---

## Datensparsamkeit:

<br><br>

### Streamen:

- Datenrate skaliert quadratisch mit Auflösung
    - Faustregel: so gering wie möglich, so hoch wie nötig
- ggf. besser runterladen statt streamen

<br>

### Allgemein

- Werbung blocken!
- Tracking reduzieren (z.B. eigenen Browser-Container für Online-Shopping)
- Nachhaltigkeit: Text > Audio > Video
- Nicht mehr benötigte Daten löschen

---

## kommerzielle Monopolstrukturen meiden

![](https://video-images.vice.com/_uncategorized/1544569811922-1524514817075-drake_tech.jpeg?resize=400:*)

Beispiele:

- [Python](https://tu-dresden.de/pythonkurs) statt Matlab
- [Nextcloud](https://nextcloud.com/) (bzw. [TUD "cloudstore" Instanz](cloudstore.zih.tu-dresden.de/)) statt dropbox
- [Matrix](https://matrix.tu-dresden.de/) statt WhatsApp, [Mastodon](https://docs.joinmastodon.org/) statt Twitter
- [HedgeDoc](https://demo.hedgedoc.org/) statt GoogleDocs, [LibreOffice](https://www.libreoffice.org/) statt MS Office
- [OpenStreetmap](https://www.openstreetmap.org/#map=16/51.0280/13.7299) statt GoogleMaps
- [Linux](https://getgnulinux.org/) statt Windows
- [Startpage](https://startpage.com) statt Google
- Buchladen statt Amazon, ...
- Passende Linksammlungen:
    - https://switching.software/
    - https://degooglisons-internet.org/de/
    - https://prism-break.org/de/ ("prism" ist der Code-Name eines der NSA-Überwachungsprogramme)

---

## Weitere Möglichkeiten (individuelle Ebene)

- Hardware möglichst lange nutzen
    - Auf Erweiterbarkeit achten
    - OpenSource Software kann viel helfen ("postMarket OS")

<br>
- Datenschutz und Datensicherheit
    - private Daten schützen → Selbstbestimmung
    - Kommunikationsgeheimnis schützen → Demokratie stärken


---

## Strukturelle Ebene (1, TU Dresden)

<br>
nachhaltiges individuelles Handeln ...

1. ... ermöglichen
    - z.B. akzeptierte Software in Lehrveranstaltungen (Dateiformate!)
    <br>
2. ... fördern
    - Nachhaltigkeitsstrategie (inkl. Digitalisierung)
    - Berücksichtigen bei Beschaffungsentscheidungen (Hardware **und** Software)
    <br>

→ AG Digitalisierung und Nachhaltigkeit: [https://tu-dresden.de/...](https://tu-dresden.de/tu-dresden/organisation/gremien-und-beauftragte/kommissionen/kommission-umwelt/die-ags-der-kommission-umwelt#ck_AG%20DiGNa)

---

## Strukturelle Ebene (2, Gesellschaft)

<br><br><br>
- (eigene und fremde) Interessenlagen klar und ehrlich analysieren
    - Beispiel:
        - Interesse TUD: Office Dokumente bearbeiten
        - Interesse Microsoft: Profit für Aktionär:innen maximieren
        <br>
- Strukturen mit Macht (z.B. Konzerne, Behörden, ...) wollen Macht erhalten und ausbauen
  <br>
- → Unvermeidlicher Konflikt zu gesamtgesellschaftlichen Interessen

<br>

- <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  →  **gesellschaftliches Engangement notwendig!**

---

## Strukturelle Ebene (3, Gesellschaft)

<br><br><br>

- ∃ zivigesellschaftliche Gruppen
    - Chaos Computer Club
    - DigitalCourage
    - ...
    - <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
      → Bits&Bäume [https://bits-und-baeume.org/](https://bits-und-baeume.org/)
      - Projekte:
          - Konferenzen organisieren
          - Lobby-Arbeit
          - AGs (z.B. zu konstruktiver digitaler Diskussionskultur)
          - lokale Gruppen (z.B. https://dresden.bits-und-baeume.org/)

- <!-- .element: class="fragment fade-up" data-fragment-index="2" -->
  Parteien (bestehende und ggf. neue)

---

## Fazit

<br><br>

- Digitalisierung birgt Chancen und Risiken
- Aktuell dominieren Risiken
    - Grund: von Menschen getroffene Entscheidungen
        - individuell und strukturell
- Damit Chancen überwiegen, müssen andere Entscheidungen getroffen werden

<br><br>

- <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  Individuelle Handlungsmöglichkeiten:
  - Probleme präzise kennen (sich selbst weiterbilden)
  - "**Fußabdruck**": Probleme individuell vermeiden (Datensparsamkeit, Werbung blockieren, Nutzungsdauer, Freie Software, ...)
  - "**Handabdruck**": Einsatz für digitale Nachaltigkeit auf strukturelle Ebene (Orga-Gremien, lokale Initiativen, NGOs, Politik)

---

## Folien verbessern

<br>
<br>
<br>



- Der Quelltext dieser Vortragsfolien ist online verfügbar
    - Prinzip: open educational ressources (OER)
    - <https://codeberg.org/TUD-DigNa/workshop-2023-digitalisierung-und-nachhaltigkeit/>
    - Verbesserungsvorschläge willkommen

---

## Diskussion


---

## Kontakt

<br><br>
<br>

- [AG Digitalisierung und Nachhaltigkeit (DigNa)](https://tu-dresden.de/tu-dresden/organisation/gremien-und-beauftragte/kommissionen/kommission-umwelt/die-ags-der-kommission-umwelt#ck_AG%20DiGNa)
  <br><br>

- digna{ät}tu-dresden.de

  <br><br>

- [https://matrix.to/#/#digna:tu-dresden.de](https://matrix.to/#/#digna:tu-dresden.de?via=tu-dresden.de&via=matrix.org)

